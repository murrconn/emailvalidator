package sheridan;


import static org.junit.Assert.*;
import org.junit.Test;

public class EmailValidatorTest {

	//getEmail
	String email = "googler@aol.com";
	String ACCOUNT_INVALID = "Account must be valid";
	String DOMAIN_INVALID = "Domain must be valid";
	String EXT_INVALID = "Extension must be valid";
	@Test
	public void testisEmailValid() {
		assertTrue("Email must be valid", EmailValidator.isEmailValid("google@gmail.com"));
	}
	@Test
	public void testisEmailValidException() {
		assertFalse("Email must not have two @@", EmailValidator.isEmailValid("googler@@.com"));
	}
	@Test
	public void testisEmailValidBoundryIn() {
		assertFalse("Email must be valid", EmailValidator.isEmailValid("sss3444sssssf44#$.s@.com"));
	}
	@Test
	public void testisEmailValidBoundryOut() {
		assertFalse("Email must be valid ", EmailValidator.isEmailValid("go23SDDFERWodsgler@.com"));
	}
	
	//accountNameValid
	
	@Test
	public void testisAccountNameValid() {
		assertTrue(ACCOUNT_INVALID, EmailValidator.isAccountNameValid("google@gmail.com"));
	}
	@Test
	public void testisAccountNameValidException() {
		assertTrue(ACCOUNT_INVALID, EmailValidator.isAccountNameValid("google123$$bnigmoney@gmail.com"));
	}
	@Test
	public void testisAccountNameValidBoundryIn() {
		assertFalse(ACCOUNT_INVALID, EmailValidator.isAccountNameValid("22#DDDDD#ss22@gmail.com"));
	}
	@Test
	public void testisAccountNameValidBoundryOut() {
		assertFalse(ACCOUNT_INVALID, EmailValidator.isAccountNameValid("223232332323@gmail.com"));
	}
	
	//domainValid
	
	@Test
	public void testisDomainValid() {
		assertTrue(DOMAIN_INVALID, EmailValidator.isDomainValid("google@gmail.com"));
	}
	@Test
	public void testisDomainValidException() {
		assertTrue(DOMAIN_INVALID, EmailValidator.isDomainValid("google@nullHostingerGoogle.com"));
	}
	@Test
	public void testisDomainValidoundryIn() {
		assertTrue(DOMAIN_INVALID, EmailValidator.isDomainValid("google@nullNULLED123.com"));
	}
	@Test
	public void testisDomainValidBoundryOut() {
		assertTrue(DOMAIN_INVALID, EmailValidator.isDomainValid("google@g2mail222.com"));
	}
	
//	//extension Valid
	
	@Test
	public void testisExtensionValid() {
		assertTrue(EXT_INVALID, EmailValidator.isExtensionValid("google@gmail.com"));
	}
	@Test
	public void testisExtensionValidException() {
		assertTrue(EXT_INVALID, EmailValidator.isExtensionValid("google@gmail.com"));
	}
	@Test
	public void testisExtensionValidBoundryIn() {
		assertFalse(EXT_INVALID, EmailValidator.isExtensionValid("google@gmail.c22343om"));
	}
	@Test
	public void testisExtensionValidBoundryOut() {
		assertFalse(EXT_INVALID, EmailValidator.isExtensionValid("google@gmail.c2323COMNm"));
	}

}
